using System;
using System.Web.Hosting;
using Eksponent.LiveCssReloader;
using System.Web.Routing;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(LessInMvcSample.App_Start.LiveCssReloaderConfig), "RegisterPaths")]
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(LessInMvcSample.App_Start.LiveCssReloaderConfig), "MapSignalRHubRoute")]

namespace LessInMvcSample.App_Start {
    public static class LiveCssReloaderConfig {
		public static void MapSignalRHubRoute()
		{
			RouteTable.Routes.MapHubs();
		}
		
		public static void RegisterPaths() {
           new LiveReloader().AddPath(HostingEnvironment.MapPath("/stylesheets/"), "styles.css").Run();
        }
    }
}