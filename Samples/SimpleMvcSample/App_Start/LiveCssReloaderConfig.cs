using System;
using System.Web.Hosting;
using Eksponent.LiveCssReloader;
using System.Web.Routing;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(SimpleMvcSample.App_Start.LiveCssReloaderConfig), "RegisterPaths")]
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(SimpleMvcSample.App_Start.LiveCssReloaderConfig), "MapSignalRHubRoute")]

namespace SimpleMvcSample.App_Start {
    public static class LiveCssReloaderConfig {
		public static void MapSignalRHubRoute()
		{
			RouteTable.Routes.MapHubs();
		}
		
		public static void RegisterPaths() {
           new LiveReloader().AddPath(HostingEnvironment.MapPath("/Content/"), "*.css").Run();
        }
    }
}