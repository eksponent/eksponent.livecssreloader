﻿var LiveReloader = (function () {
    var logEnabled = false,
        reloadAllCss = function (postfix) {
            $('link[rel="stylesheet"]').each(function (i, e) {
                var p = $(e).attr('href');
                var parts = p.split('?');
                $(e).attr('href', parts[0] + '?' + postfix);
            });
        },
        log = function(msg) {
          if (logEnabled && window.console) {
              console.log(msg);
          }  
        },
        init = function () {
            var hub = $.connection.liveReload;
            hub.client.reloadAllCSS = function (postfix) {
                log('reload style');
                reloadAllCss(postfix);
            };
            $.connection.hub.start().done(function () { });
        };
    return { 'init': init };
})();

LiveReloader.init();