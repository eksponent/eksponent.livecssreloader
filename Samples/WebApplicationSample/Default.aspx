﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplicationSample.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="MyStyle.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Web Application Sample</h1>
        <p>LiveCssReloader will also work in Web Forms</p>
    </div>
    </form>
    <script src="Scripts/jquery-1.6.4.min.js"></script>
    <script src="/Scripts/jquery.signalR-1.1.2.min.js"></script>
    <script src="/signalr/hubs"></script>
    <script src="/Scripts/LiveReloader.js"></script>
</body>
</html>
