using System;
using System.Web.Hosting;
using Eksponent.LiveCssReloader;
using System.Web.Routing;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(WebApplicationSample.App_Start.LiveCssReloaderConfig), "RegisterPaths")]
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(WebApplicationSample.App_Start.LiveCssReloaderConfig), "MapSignalRHubRoute")]

namespace WebApplicationSample.App_Start {
    public static class LiveCssReloaderConfig {
		public static void MapSignalRHubRoute()
		{
			RouteTable.Routes.MapHubs();
		}
		
		public static void RegisterPaths() {
           new LiveReloader().AddPath(HostingEnvironment.MapPath("/"), "*.css").Run();
        }
    }
}