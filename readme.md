LiveCssReloader
==================
This is a small experimental project for improving the web development workflow in Asp.NET.

It will dynamically reload stylesheets in all open browsers when style sheets are changed on disk.

When is this handy?
-------------------

* When you have arrived to that point where you're primarily working with the stylesheet and you don't want to jump between chrome dev tools, editor and CTRL-R all the time.
[Short demo](http://www.screenr.com/LRUH)
* When you are pinching pixels and need to look at the results across several browsers at the same time.
[40 sec demo](http://www.screenr.com/wzUH)
* When you are hacking away at your responsive design and need to see several screen sizes at the same time.
* It works on your smartphone. That's just ... smart.


Usage
-------------------
1. Start website.
2. Make a change in your Site.css file.
3. Save.
4. Observe stylechange in browser without reload.

See how it works:

* [Simplest case CSS reload](http://www.screenr.com/LRUH)
* [Updating Chrome, Firefox and IE simultanously on LESS style sheet changes](http://www.screenr.com/wzUH)



Installation
-----------------

First install with NuGet:

  PM> Install-Package LiveCssReloader


Then add the following lines before the end of your mvc layout page / master page.:

    <script src="~/Scripts/jquery.signalR-1.1.2.min.js"></script>
    <script src="/signalr/hubs"></script>
    <script src="~/Scripts/LiveReloader.js"></script>

Note that the ~ character should be omitted in a web forms application, and the version of SignalR javascript files might be higher in your application.

Samples
--------------------
Samples are included for MVC and Webforms as well as the LESS compiler included in VS2012 update 2

Configuration
--------------------
Modify or add more paths to the configuration in LiveCssReloaderConfig.cs in the App_Start folder.

Troubleshooting
--------------------

1. Check the javascript console for errors. This is where you will see red lines in case of a malconfigured signalR connection.
2. Set enableLog to true in LiveReloader.js and see if the event is triggered from the backend
3. Add a non-existing path in LiveCssReloaderConfig.cs and make sure that you get an exception on startup. If not, the WebActivator is not running at startup.
The easiest solution to this is to call the two methods directly at *the beginning* of the Application_Start callback in Global.asax class.



Already using SignalR?
--------------------
If you are already using SignalR in your app, you should remove the following line from LiveCssReloaderConfig.cs:
		
	RouteTable.Routes.MapHubs();






