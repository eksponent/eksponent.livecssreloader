using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Eksponent.LiveCssReloader
{
	public class LiveReloader
	{
		private static IHubConnectionContext _clients;
		private static readonly List<string[]> Paths = new List<string[]>();

		public LiveReloader AddPath(string directory, string pattern)
		{
			Paths.Add(new[] { directory, pattern });
			return this;
		}

		public void Run()
		{
			foreach (var paths in Paths)
			{
				FileSystemWatcher fileSystemWatcher;
				try
				{

					fileSystemWatcher = new FileSystemWatcher(paths[0], paths[1]);
				}
				catch (ArgumentException e)
				{
					throw new IllegalPathException("Error while registering the CSS path to monitor. Are you sure this is where your stylesheets live?",e);
				}
				fileSystemWatcher.Changed += (sender, args) =>
					{
						_clients = GlobalHost.ConnectionManager.GetHubContext<LiveReloadHub>().Clients;
						_clients.All.reloadAllCSS(Guid.NewGuid().ToString());
					};

					fileSystemWatcher.EnableRaisingEvents = true;
			}
		}


	}
	public class IllegalPathException :Exception
	{
		public IllegalPathException(string msg, Exception inner):base(msg,inner){}
	}
}