LiveCssReloader
==================

This is a small experimental project for improving the web development workflow in Asp.NET.

Usage
-------------------
1. Start website.
2. Make a change in your Site.css file.
3. Save.
4. Observe stylechange in browser without reload.


How to use
==================

Install with NuGet:

  PM> Install-Package LiveCssReloader

Post installation in MVC 4
------------------

Add in mvc layout page:

    <script src="~/Scripts/jquery.signalR-1.1.2.min.js"></script>
    <script src="/signalr/hubs"></script>
    <script src="~/Scripts/LiveReloader.js"></script>

Note that the ~ character should be omitted in a web forms application, and the version of SignalR javascript files might be higher in your application.


Configuration
--------------------
Modify or add more paths to the configuration in LiveCssReloaderConfig.cs in the App_Start folder.

Troubleshooting
--------------------

1. Check the javascript console for errors. This is where you will see red lines in case of a malconfigured signalR connection.
2. Set enableLog to true in LiveReloader.js and see if the event is triggered from the backend
3. Add a non-existing path in LiveCssReloaderConfig.cs and make sure that you get an exception on startup. If not, the WebActivator is not running at startup.
The easiest solution to this is to call the two methods directly at *the beginning* of the Application_Start callback in Global.asax class.




Already using SignalR?
--------------------
If you are already using SignalR in your app, you should remove the following line from LiveCssReloaderConfig.cs:
		
	RouteTable.Routes.MapHubs();

Find more at https://bitbucket.org/eksponent/eksponent.livecssreloader